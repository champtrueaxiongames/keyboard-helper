﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TestKeyboardProject
{
    public class TestKeyboard : MonoBehaviour
    {
        [SerializeField]
        private Button openKeyboardButton = default;

        [SerializeField]
        private ScrollRect scrollRect = default;

        [SerializeField]
        private RectTransform mainContentRect = default;

        [SerializeField]
        private Canvas canvas = default;

        [SerializeField]
        private RectTransform offsetButtom = default;

        [SerializeField]
        private TextMeshProUGUI debugResultText = default;

        private bool isAdjusting = false;

        private void Start()
        {
            openKeyboardButton.onClick.AddListener(OnOpenKeyboardButtonClicked);
        }

        private async void OnOpenKeyboardButtonClicked()
        {
            isAdjusting = true;
            var keyboard = await KeyboardHelper.OpenKeyboardAsync(
                text: "",
                keyboardType: TouchScreenKeyboardType.Default,
                isAutoCorrection: false,
                isMultiline: false,
                isSecure: false,
                isAlert: false,
                textPlaceholder: "<input something>",
                characterLimit: 20
            );

            debugResultText.text = keyboard.text;
            isAdjusting = false;
            OffsetScrollView(0);
        }

        private void OffsetScrollView(int height)
        {
            mainContentRect.offsetMin = new Vector2(mainContentRect.offsetMin.x, height);
            scrollRect.normalizedPosition = new Vector2(0, 0);
        }

        private void UpdateOffset()
        {
            int keyboardHeight = KeyboardHelper.GetRelativeKeyboardHeight(
                canvasRect: canvas.GetComponent<RectTransform>(),
                includeInputField: true
            );
            int offsetHeight = keyboardHeight - Mathf.CeilToInt(offsetButtom.rect.height);
            OffsetScrollView(offsetHeight);
        }

        private void Update()
        {
            if (isAdjusting)
            {
                UpdateOffset();
            }
        }
    }
}

using System;
using System.Threading.Tasks;
using UnityEngine;

namespace TestKeyboardProject
{
    public static class KeyboardHelper
    {
        public static async void OpenKeyboard(
            string text,
            TouchScreenKeyboardType keyboardType,
            bool isAutoCorrection,
            bool isMultiline,
            bool isSecure,
            bool isAlert,
            string textPlaceholder,
            int characterLimit,
            Action<TouchScreenKeyboard> inputTextCallback
        )
        {
            TouchScreenKeyboard keyboard = await OpenKeyboardAsync(
                text: text,
                keyboardType: keyboardType,
                isAutoCorrection: isAutoCorrection,
                isMultiline: isMultiline,
                isSecure: isSecure,
                isAlert: isAlert,
                textPlaceholder: textPlaceholder,
                characterLimit: characterLimit
            );
            inputTextCallback?.Invoke(keyboard);
        }

        public static async Task<TouchScreenKeyboard> OpenKeyboardAsync(
            string text,
            TouchScreenKeyboardType keyboardType,
            bool isAutoCorrection,
            bool isMultiline,
            bool isSecure,
            bool isAlert,
            string textPlaceholder,
            int characterLimit
        )
        {
            TouchScreenKeyboard keyboard = TouchScreenKeyboard.Open(
                   text: text,
                   keyboardType: keyboardType,
                   autocorrection: isAutoCorrection,
                   multiline: isMultiline,
                   secure: isSecure,
                   alert: isAlert,
                   textPlaceholder: textPlaceholder,
                   characterLimit: characterLimit
           );

            while (keyboard != null && keyboard.active)
            {
                await Task.Yield();
            }

            return keyboard;
        }

        public static int GetRelativeKeyboardHeight(RectTransform canvasRect, bool includeInputField)
        {
            int keyboardHeight = GetKeyboardHeightInPixel(includeInputField);
            float screenToRectRatio = Screen.height / canvasRect.rect.height;
            float keyboardHeightRelativeToRect = keyboardHeight / screenToRectRatio;

            return Mathf.CeilToInt(keyboardHeightRelativeToRect);
        }

        public static int GetKeyboardHeightInPixel(bool includeInputField)
        {
#if UNITY_EDITOR
            return 0;
#elif UNITY_ANDROID
            using (AndroidJavaClass unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                AndroidJavaObject currentActivity = unityClass.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject unityPlayer = currentActivity.Get<AndroidJavaObject>("mUnityPlayer");
                AndroidJavaObject gameView = unityPlayer.Call<AndroidJavaObject>("getView");
                if (gameView != null)
                {
                    int inputFieldHeight = 0;
                    if (includeInputField)
                    {
                        AndroidJavaObject dialog = unityPlayer.Get<AndroidJavaObject>("mSoftInputDialog");
                        if (dialog != null)
                        {
                            AndroidJavaObject window = dialog.Call<AndroidJavaObject>("getWindow");
                            AndroidJavaObject decorView = window.Call<AndroidJavaObject>("getDecorView");
                            if (decorView != null)
                            {
                                inputFieldHeight = decorView.Call<int>("getHeight");
                            }
                        }
                    }
                    using (AndroidJavaObject rect = new AndroidJavaObject("android.graphics.Rect"))
                    {
                        gameView.Call("getWindowVisibleDisplayFrame", rect);
                        int gameViewHeight = rect.Call<int>("height");
                        return Screen.height - gameViewHeight + inputFieldHeight;
                    }
                }
                return 0;
            }
#elif UNITY_IOS
        return Mathf.CeilToInt(TouchScreenKeyboard.area.height);
#endif
        }
    }
}
